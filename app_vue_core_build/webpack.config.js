// Lib imports
const { spawn } = require('child_process');
const pathLib = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MediaQueryPlugin = require('media-query-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const WebpackPreBuildPlugin = require('pre-build-webpack');
const WebpackOnBuildPlugin = require('on-build-webpack');
const moment = require('moment');
const momentDurationFormatSetup = require('moment-duration-format');
const EslintFriendlyFormatter = require('eslint-friendly-formatter');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const dwAutoUpload = require('./dwAutoUpload');
const configMegre = require('@tridnguyen/config');
const nodeExternals = require('webpack-node-externals');

// relative path to "cartridges" directory
const cartridgesPath = '../cartridges';
// target cartridge name
const cartridgeName = 'app_vue_core';

// dev-specific config, each developer can create "developerBuildPrefs.json" file with custom settings
const developerConfig = configMegre({
    enablePostBuildTimer: true,
    enableAutoUpload: false,
    alwaysGzip: false
}, './developerBuildPrefs.json');

// init easy formatting for moment "duration" objects
momentDurationFormatSetup(moment);

const ENV_DEV = 'development';
const ENV_STAGING = 'staging';
const ENV_PROD = 'production';
const ENV_TEST = 'test';
const ENV_BUNDLE_PROFILING = 'profiling';

// Explicit checks for current environment
const isDevelopment = process.env.NODE_ENV === ENV_DEV;
const isStaging = process.env.NODE_ENV === ENV_STAGING;
const isProduction = process.env.NODE_ENV === ENV_PROD;
const isTesting = process.env.NODE_ENV === ENV_TEST;
const isBundleProfiling = process.env.NODE_ENV === ENV_BUNDLE_PROFILING;

// Will "moment.js" lib will be used on a project?
const useMoment = true;

// Enable JS linting?
const enableJsLinter = isDevelopment || isStaging;
const jsLintExcludes = /node_modules|some_other_path/;

// Enable SCSS linting?
const enableScssLinter = isDevelopment || isStaging;
const ScssLintWatchPaths = [`${cartridgesPath}/**/${cartridgeName}/**/*.s?(a|c)ss`];

// Source maps configuration
const sourceMapDevToolPluginIgnoreList = ['js/webpack-runtime.js'];
if (!isBundleProfiling) {
    sourceMapDevToolPluginIgnoreList.push('js/vendor.js');
}

//  "Watch" mode runtime variables
let buildCounter = 0;
let lastBuildStartDate;
let lastBuildPassTime;

console.log(`
    Failsafe checks:
    isDevelopment is ${isDevelopment}
    isStaging is ${isStaging}
    isProduction is ${isProduction}
    isTesting is ${isTesting}
    isBundleProfiling is ${isBundleProfiling}
`);

// start gulp files watch to auto-upload files to SB and auto-refresh corresponding Chrome tabs
if (isDevelopment && developerConfig.enableAutoUpload) {
    dwAutoUpload.startWatch();
}

// Webpack configuration for current cartridge
module.exports = {
    name: 'New Vue.js application',
    mode: (isDevelopment || isStaging) ? ENV_DEV : ENV_PROD,
    entry: {
        [`${cartridgeName}-bundle`]: pathLib.resolve(__dirname, `${cartridgesPath}/${cartridgeName}/cartridge/js/index.js`),
    },
    output: {
        filename: 'js/[name].js',
        chunkFilename: 'js/[name].js',
        path: pathLib.resolve(__dirname, `${cartridgesPath}/${cartridgeName}/cartridge/static/default`),
        pathinfo: isDevelopment
    },
    module: {
        rules: [
            // ESLint loader
            enableJsLinter ? {
                enforce: 'pre',
                test: /\.(js|vue)$/,
                exclude: jsLintExcludes,
                use: [
                    {
                        loader: 'eslint-loader',
                        options: {
                            configFile: '.eslintsrc.js',
                            formatter: EslintFriendlyFormatter,
                            emitError: true,
                            emitWarning: true
                        }
                    }
                ]
            } : null,
            // Babel loader
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        // babel config is read from ".babelrc"
                        loader: 'babel-loader?cacheDirectory'
                    },
                ]
            },
            // Vue single file components loader
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true
                }
            },
            {
                test: /\.css$/,
                use: isTesting
                    ? ['null-loader']
                    : [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.scss$/,
                use: isTesting
                    ? ['null-loader']
                    : [
                        {
                            loader: MiniCssExtractPlugin.loader
                        },
                        {
                            loader: 'css-loader', // translates CSS into CommonJS
                            options: {
                                sourceMap: true,
                                url: false,
                                importLoaders: 1
                            }
                        },
                        // TODO: uncomment when implemented https://github.com/SassNinja/media-query-plugin/issues/11
                        // {
                        //     loader: MediaQueryPlugin.loader
                        // },
                        {
                            loader: 'postcss-loader',
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    require('postcss-import')(),
                                    require('postcss-cssnext')({
                                        features: {
                                            customProperties: { warnings: false }
                                        }
                                    }),
                                    require('postcss-font-magician')()
                                ],
                                sourceMap: true,
                                config: {
                                    path: './'
                                }
                            }
                        },
                        {
                            loader: 'sass-loader', // compiles Sass to CSS
                            options: {
                                sourceMap: true
                            }
                        },
                    ]
            }
        ].filter(Boolean),
    },
    optimization: {
        minimize: isStaging || isProduction,
        minimizer: [
            // we specify a custom UglifyJsPlugin here to get source maps in production
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                uglifyOptions: {
                    compress: false,
                    ecma: 6,
                    mangle: true
                },
                sourceMap: isStaging
            })
        ],
        runtimeChunk: { name: 'webpack-runtime' },
        splitChunks: {
            cacheGroups: {
                default: false,
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all'
                },
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    plugins: [
        new VueLoaderPlugin(),
        (isDevelopment || isStaging || isBundleProfiling)
            ? new webpack.SourceMapDevToolPlugin({
                filename: '[file].map',
                exclude: sourceMapDevToolPluginIgnoreList
            })
            : null,
        (developerConfig.alwaysGzip || isStaging || isProduction)
            ? new CompressionPlugin({
                filename: '[path].gz[query]',
                algorithm: 'gzip',
                test: /\.js$/,
                threshold: 10240,
                minRatio: 0.8
            })
            : null,
        // exclude development-specific code blocks for production build
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: `"${process.env.NODE_ENV}"` || `"${ENV_DEV}"`
            }
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
        }),
        // TODO: uncomment when implemented https://github.com/SassNinja/media-query-plugin/issues/11
        // new MediaQueryPlugin({
        //     include: true,
        //     queries: {
        //         '(min-width: 1024px)': 'desktop'
        //     },
        //     groups: {
        //         'app-test-vue-core-bundle': true
        //     }
        // }),
        // Removes all locale bundles from Moment except given: greatly reduces bundle size
        useMoment
            ? new MomentLocalesPlugin({
                // "en" locale is built into Moment and can’t be removed
                localesToKeep: ['de', 'fr']
            })
            : null,
        enableScssLinter
            ? new StyleLintPlugin({
                configFile: './.stylelintrc.json',
                files: ScssLintWatchPaths
            })
            : null,
        isDevelopment
            ? new WebpackPreBuildPlugin(function () {
                clearInterval(lastBuildPassTime);
                console.log('\n');
                console.log(`----------------- Webpack build #${++buildCounter} started at ${moment().format('HH:mm:ss')} ------------------------`);
            })
            : null,
        (isDevelopment || isBundleProfiling)
            ? new WebpackOnBuildPlugin(function () {
                if (isDevelopment && developerConfig.enablePostBuildTimer) {
                    lastBuildStartDate = moment();

                    setTimeout(function () {
                        // just to get a new line break
                        console.log('');

                        lastBuildPassTime = setInterval(function () {
                            const duration = moment.duration(moment().diff(lastBuildStartDate));

                            process.stdout.clearLine();
                            process.stdout.cursorTo(0);
                            process.stdout.write(`------------------------- ${duration.format()} passed from last build -------------------------`);
                        }, 1000);
                    }, 1000);
                }

                if (isBundleProfiling) {
                    // run browser with visual representation right after the build have finished
                    spawn('node', [
                        './node_modules/source-map-explorer/index.js',
                        `${cartridgesPath}/${cartridgeName}/cartridge/static/default/js/vendor.js`
                    ]);
                }
            })
            : null,
    ].filter(Boolean),
    watch: isDevelopment,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 300
    },
    resolve: {
        modules: [
            pathLib.resolve(`${cartridgesPath}/app_vue_core/cartridge`),
            pathLib.resolve('./node_modules'),
        ],
        alias: {
            // aliases will work for imports even in single file Vue components,
            // and in Mocha test files too!
            [`@${cartridgeName}_js`]: pathLib.resolve(`${cartridgesPath}/${cartridgeName}/cartridge/js`),
            [`@${cartridgeName}_scss`]: pathLib.resolve(`${cartridgesPath}/${cartridgeName}/cartridge/scss`),
            // sane pathes for cross-cartridge imports
            '@cartridges': pathLib.resolve(cartridgesPath)
        }
    },
    externals: [isTesting && nodeExternals()].filter(Boolean),
    // what info to show after each build
    stats: {
        // Add asset information
        assets: true,
        // Add built modules information
        modules: false,
        // Add chunk information (setting this to `false` allows for a less verbose output)
        chunks: false,
        // Add information about cached (not built) modules
        cached: true,
        // Add warnings
        warnings: true,
        // Show performance hint when file size exceeds `performance.maxAssetSize`
        performance: true,
        // Display the entry points with the corresponding bundles
        entrypoints: false,
        // Add errors
        errors: true,
        // Add details to errors (like resolving log)
        errorDetails: true,
        // Add public path information
        publicPath: true,
        // Add timing information
        timings: true,
    }
};
