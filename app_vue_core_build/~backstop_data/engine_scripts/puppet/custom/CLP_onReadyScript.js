const utils = require('./utils');
// const footerSelector = 'footer';

module.exports = async (page, scenario, vp) => {
    // await page.waitFor(footerSelector);
    // const footer = await page.$(footerSelector);
    // await footer.focus();
    utils.injectLogCurrentTestInfo(scenario, vp);

    await page.evaluate(function () {
        window.scrollBy(0, document.body.scrollHeight);
    });
};
