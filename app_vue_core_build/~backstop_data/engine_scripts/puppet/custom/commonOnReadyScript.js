const utils = require('./utils');

const scenariosCount = 3;
const breakpointsCount = 3;

module.exports = async (page, scenario, vp) => {
    utils.injectLogCurrentTestInfo(scenario, vp);

    if (scenario.sIndex === scenariosCount - 1 && vp.vIndex === breakpointsCount - 1) {
        utils.enableLogging();
    }
};
