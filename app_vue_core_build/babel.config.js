module.exports = {
    presets: [
        ['@babel/preset-env', {
            targets: {
                browsers: ['last 2 versions', 'ie >= 10']
            },
            modules: false,
            useBuiltIns: false,
            loose: true
        }]
    ],
    plugins: [
        '@babel/plugin-transform-async-to-generator',
        '@babel/plugin-transform-spread',
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-proposal-optional-chaining'
    ],
    comments: true
};
