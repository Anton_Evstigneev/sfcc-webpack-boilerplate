const gulp = require('gulp');
const path = require('path');
// "auto-uploading" related imports
const dwdav = require('dwdav');
const config = require('@tridnguyen/config');
const gutil = require('gulp-util');
// Chrome plugin must be installed (https://chrome.google.com/webstore/detail/tab-auto-reloader/knnahnemielbnanghaphjgheamgcjjcb)
const ChromeTabReloader = require('chrome-tab-reloader');

// module state
let uploadCount = 0;
let changesCount = 0;

const filesWatchPattern = [
    './cartridges/app_vue_core/cartridge/**/*.{isml,json,properties,xml,js,ds,css,png,gif,jpg,map,svg}'
];

function upload(files, tabReloaderInstance) {
    var credentials = config('dw.json', { caller: false });
    var server = dwdav(credentials);

    Promise.all(files.map(function (file) {
        return server.post(path.relative(process.cwd(), file));
    })).then(function () {
        uploadCount++;
        gutil.log(gutil.colors.green('Uploaded ' + files.join(',') + ' to the server'));
        if (uploadCount === changesCount) {
            tabReloaderInstance(files);
        }
    }).catch(function (err) {
        gutil.log(gutil.colors.red('Error uploading ' + files.join(','), err));
    });
}

function startWatch() {
    console.log('starting auto-upload');
    const tabReloaderInstance = new ChromeTabReloader({ port: 8001 });

    gulp.watch(filesWatchPattern, { interval: 500 }, function (event) {
        changesCount++;

        setTimeout(function () {
            upload([event.path], tabReloaderInstance);
        }, 500);
    });
}

exports.startWatch = startWatch;