/**
 * Helper for Leadtime integration
 *
 * @module integrations/LeadtimeHelper
 */

'use strict';

/* API Includes */
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var Resource = require('dw/web/Resource');
var ContentMgr = require('dw/content/ContentMgr');

/* Script Modules */
var StocksHelper = require('app_celine_core/cartridge/scripts/util/StocksHelper.js');
var SiteHelpers = require('app_celine_core/cartridge/scripts/util/SiteHelpers.js');
var STOCK_CONSTANTS = StocksHelper.getConstants();

/** @constructs LeadtimeHelper */
function LeadtimeHelper() {}

/* Constants */
LeadtimeHelper.SPECIAL_FORMATS = ['dayWithSuffix'];

/**
 * @function getDayNumberSuffix
 * @description Get suffix day number
 * @param {Number} day
 * @returns {String}
 */
function getDayNumberSuffix(day) {
    if (day >= 11 && day <= 13) {
        return 'th';
    }

    switch (day % 10) {
        case 1:
            return 'st';
        case 2:
            return 'nd';
        case 3:
            return 'rd';
        default:
            return 'th';
    }
}

var formatWithSpecial = {

    /**
     * @description Format date with days that includes suffix
     * @param {Date} value
     * @returns {String}
     */
    dayWithSuffix: function(value, showSuffix) {
        var calendar = new Calendar(value),
            showSuffix = (typeof showSuffix == 'undefined') ? true : showSuffix,
            dayFormatted = StringUtils.formatCalendar(calendar, 'd'),
            daySuffix = showSuffix ? getDayNumberSuffix(calendar.get(Calendar.DAY_OF_MONTH)) : '',
            monthFormatted = StringUtils.formatCalendar(calendar, 'MMM');

        return dayFormatted + daySuffix + ' ' + monthFormatted;
    }
}

/**
 * @function formatDate
 * @description Format date in specified format
 * @param {Date} value
 * @param {String} dateFormat
 * @returns {String|Null}
 */
function formatDate(value, dateFormat, showSuffix) {
    var specialFormats = LeadtimeHelper.SPECIAL_FORMATS;

    try {
        if (specialFormats.indexOf(dateFormat) !== -1 && dateFormat in formatWithSpecial) {
            return formatWithSpecial[dateFormat](value, showSuffix);
        }

        return StringUtils.formatCalendar(new Calendar(value), dateFormat);
    } catch (e) {
        return null;
    }
}

/**
 * @function format
 * @description Format leadtime based on type
 * @param {String|Date} leadtime
 * @param {String} dateFormat
 * @returns {String|Null}
 */
LeadtimeHelper.prototype.format = function(leadtime, dateFormat, showSuffix) {
    if (typeof leadtime == 'string' && leadtime) {
        return leadtime;
    } else if (leadtime instanceof Date && dateFormat) {
        return formatDate(leadtime, dateFormat, showSuffix);
    } else {
        return null;
    }
};

/**
 * @function getMaxDate
 * @description Returns a date three days from now (used to deliveries that range from 1 to 3 days)
 * @returns {String}
 */
LeadtimeHelper.prototype.getMaxDate = function() {

    var max = new Date();
    max.setTime(max.getTime() + 3 * 24 * 60 * 60 * 1000)

    return formatDate(max, "yyyy-MM-dd");
};

/**
 * @function createAttribute
 * @description Creates data attribute for shipping methods radio
 * @param {String} shippingMethodID
 * @param {Object} stores
 * @param {Object} leadtimes
 * @retunrs {String}
 */
LeadtimeHelper.prototype.createAttribute = function(shippingMethodID, stores, leadtimes) {

    var attributeStr = '';

    for (let i = 0; i < stores.length; i++) {

        var store = stores[i];
        var shippingId = shippingMethodID + store.ID;

        // Look for the store data inside leadtimes Array
        for (let j = 0; j < leadtimes.length; j++) {
            if (leadtimes[j].shippingId == shippingId) {
                attributeStr = attributeStr + ' data-leadtime-' + store.ID + '=' + this.format(leadtimes[j].deliveryDate, "yyyy-MM-dd");
                break;
            }
        }
    }

    return attributeStr;
};

/**
 * @function createCeCDescription
 * @description Creates string description for leadtimes in each store for CSC
 * @param {String} shippingMethodID
 * @param {Object} stores
 * @param {Object} leadtimes
 * @retunrs {String}
 */
LeadtimeHelper.prototype.createCeCDescription = function(shippingMethodID, stores, leadtimes) {

    var leadtimeStr = [];

    for (let i = 0; i < stores.length; i++) {

        var store = stores[i];
        var shippingId = shippingMethodID + store.ID;

        leadtimeStr.push(shippingId);
        // Look for the store data inside leadtimes Array
        for (let j = 0; j < leadtimes.length; j++) {
            if (leadtimes[j].shippingId == shippingId) {
                leadtimeStr.push(' Available in ' + store.name + '(' + store.ID + ') starting on ' + this.format(leadtimes[j].deliveryDate, "yyyy-MM-dd"));
                break;
            }
        }
    }

    return leadtimeStr;
};

/**
 * @function getLeadtimeMessage
 * @description Creates leadtime message
 * @param {String|Date} leadtime
 * @param {String} leadtimeFormatted
 * @retunrs {String}
 */
LeadtimeHelper.prototype.getLeadtimeMessage = function(leadtime, leadtimeFormatted, preOrderBasket) {
    if (preOrderBasket) {
        var estimatedDeliveryObject = SiteHelpers.getMonthYearByDate(leadtime);
        var estimatedDeliveryMonthStr = Resource.msg('month.' + estimatedDeliveryObject.month.toLowerCase(), 'forms', null);
        return Resource.msgf('global.preorder.estimateddelivery', 'locale', null, estimatedDeliveryMonthStr, estimatedDeliveryObject.year);
    } else {
        if (typeof leadtime == 'string' && leadtime) {
            return leadtimeFormatted;
        } else if (leadtime instanceof Date) {
            return Resource.msgf('shipment.leadtime', 'checkout', null, leadtimeFormatted);
        }
    }
};
/**
 * @function getCollectMessage
 * @description Message to be displayed on store picking confirmation
 * @param {String|Date} leadtime
 * @returns {String|Null}
 */
LeadtimeHelper.prototype.getCollectMessage = function(leadtime) {
    if (typeof leadtime == 'string' && leadtime) {
        return leadtime;
    } else if (leadtime instanceof Date) {

        var today = new Date().getDate(),
            dayNumber = StringUtils.formatCalendar(new Calendar(leadtime), 'dd');

        var deliversToday = (today == dayNumber);

        var dayStr = StringUtils.formatCalendar(new Calendar(leadtime), 'EEEEE');

        var time = leadtime.getHours();
        var hours = time > 12 ? time - 12 : time;
        var am_pm = time >= 12 ? " PM" : " AM";

        hour = hours + am_pm;

        var asset = ContentMgr.getContent('collect-in-store-stock-available-info-ca').custom.body;

        return asset;
    } else {
        return null;
    }
};

/**
 * @function getLeadtimeInfoMessage
 * @description Creates leadtime info message
 * @param {String} shippingMethodID
 * @retunrs {String}
 */
LeadtimeHelper.prototype.getLeadtimeInfoMessage = function(shippingMethodID) {
    var infoMessage = '';
    if (shippingMethodID == STOCK_CONSTANTS.METHOD_ID_CONCIERGE) {
        infoMessage = Resource.msg('singleshipping.shippinginfomessage.concierge', 'checkout', null);
    }
    return infoMessage;

};
/* Exports of the modules */

module.exports = new LeadtimeHelper();