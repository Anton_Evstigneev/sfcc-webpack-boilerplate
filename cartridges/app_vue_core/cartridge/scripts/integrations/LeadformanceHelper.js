'use strict'

/**
 * Helper that provides communication between SFCC and Leadformance
 */

/* API includes */
var Net = require('dw/net');
var Svc = require('dw/svc');
var Site = require('dw/system/Site');
var StoreMgr = require('dw/catalog/StoreMgr');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');

function format24hToAMPM (hourValue) {
    let newHour = hourValue,
        dayPart = hourValue < 11 ? 'am' : 'pm';

    if (hourValue === 0) {
        newHour = 12;
        dayPart = 'am';
    }

    if (hourValue > 12) {
        newHour = hourValue - 12;
    }

    return {
        hour: newHour,
        dayPart: dayPart,
        formattedValue: newHour + dayPart
    }
}

function formatLocaleTime(UTCHours, currentLocale) {

    var localeTimeFormatters = {
        'fr_FR': function () {
            return UTCHours + 'h'
        },
        'en_FR': function () {
            var h = format24hToAMPM(UTCHours);
            return h.hour + Resource.msg('global.time.' + h.dayPart, 'locale', null);
        },
        // default time format is AM/PM
        default: function () {
            var h = format24hToAMPM(UTCHours);
            return h.hour + Resource.msg('global.time.' + h.dayPart, 'locale', null);
        }
    };

    return localeTimeFormatters[currentLocale] ? localeTimeFormatters[currentLocale]() : localeTimeFormatters['default']();
}

/**
 *  Leadformance is used in order to retrieve list of the stores for the Find In Store functionality.
 *  It support search stores with using zipCode/countryCode and via geolocation.
 */
function LeadformanceHelper() {
    this.apiKey = !empty(Site.current.getPreferences()) ? Site.current.getCustomPreferenceValue('celLeadformanceApiKey') : '';
    /**
     * Getting store information from store with provided customId
     * @param {String} countryCode
     * @param {String} customId
     * @returns {Object} containing store information
     */
    this.getStore = function(countryCode, customId) {
        var stores = this.getStores(null, countryCode, customId).stores,
            storeJSON;

        if (stores.length) {
            storeJSON = {
                'name': stores[0]['name'],
                'address': stores[0]['address'],
                'city': stores[0]['city'],
                'phone': stores[0]['phone'],
                'postalCode': stores[0]['postal_code'],
                'hours': JSON.stringify(stores[0]['opening_hours']),
            }
        }

        return storeJSON;
    }
    /**
     * Getting store opening hours from store with provided customId
     * @param {String} countryCode
     * @param {String} customId
     * @returns {String} a string containing the hour.
     */
    this.getOpenHours = function(countryCode, customId) {
        var store = this.getStore(countryCode, customId);
        if (store == null) {
            return;
        }
        var storeHours = JSON.parse(store.hours);
        var openHour = "";
        var fromMondayToSaturday = Resource.msg('product.findinstore.storeopen.mondaysaturday', 'product', null);
        var fromMondayToSunday = Resource.msg('product.findinstore.storeopen.mondaysunday', 'product', null);
        var daysHours = [];
        for (var i = 0; i < storeHours.length; i++) {
            daysHours[storeHours[i].day_of_week] = storeHours[i];
            daysHours[storeHours[i].day_of_week].start = daysHours[storeHours[i].day_of_week].start.replace(/-/g, '/');
            daysHours[storeHours[i].day_of_week].end = daysHours[storeHours[i].day_of_week].end.replace(/-/g, '/');
            daysHours[storeHours[i].day_of_week].start = daysHours[storeHours[i].day_of_week].start.replace(/((Z)|(T))+/g, ' ');
            daysHours[storeHours[i].day_of_week].end = daysHours[storeHours[i].day_of_week].end.replace(/((Z)|(T))+/g, ' ');
        }
        var saturdayStart = new Date(daysHours[6].start);
        var saturdayEnd = new Date(daysHours[6].end);
        var mondayStart = new Date(daysHours[1].start);
        var mondayEnd = new Date(daysHours[1].end);
        var currentLocale = request.getLocale();

        var dayOfWeek = [
            Resource.msg('product.shipping.sunday', 'product', null),
            Resource.msg('product.shipping.monday', 'product', null),
            Resource.msg('product.shipping.tuesday', 'product', null),
            Resource.msg('product.shipping.wednesday', 'product', null),
            Resource.msg('product.shipping.thursday', 'product', null),
            Resource.msg('product.shipping.friday', 'product', null),
            Resource.msg('product.shipping.saturday', 'product', null)
        ];

            if (mondayStart.getUTCHours() == saturdayStart.getUTCHours() && mondayEnd.getUTCHours() == saturdayEnd.getUTCHours()) {
                var h1 = formatLocaleTime(mondayStart.getUTCHours(), currentLocale),
                    h2 = formatLocaleTime(saturdayEnd.getUTCHours(), currentLocale);

                openHour = fromMondayToSaturday + " " + h1 + " - " + h2;
            } else if (storeHours.length >= 6) {
                var sundayStart = new Date(daysHours[0].start);
                var sundayEnd = new Date(daysHours[0].end);
                var h1 = formatLocaleTime(sundayStart.getUTCHours(), currentLocale),
                    h2 = formatLocaleTime(sundayEnd.getUTCHours(), currentLocale);

                if (mondayStart.getUTCHours() == sundayStart.getUTCHours() && mondayEnd.getUTCHours() == sundayEnd.getUTCHours()) {
                    openHour = fromMondayToSunday + " " + h1 + " - " + h2;
                }
            } else {
                for (var i = 0; i < storeHours.length; i++) {
                    var hoursStart = new Date(storeHours[i].start);
                    var hoursEnd = new Date(storeHours[i].end);
                    var day = storeHours[i].day_of_week;
                    var h1 = formatLocaleTime(hoursStart.getUTCHours(), currentLocale),
                        h2 = formatLocaleTime(hoursEnd.getUTCHours(), currentLocale);

                    openHour =  "<p>" + dayOfWeek[day] + ": " + h1 + " - " + h2 + " </p>";
                }
            }
            return openHour;
    }
    /**
     * Getting stores from API services using postal code, city name, state name or country code.
     * @param {String} query
     * @param {String} countryCode
     * @returns {Array} an array containing stores.
     */
    this.getStores = function(query, countryCode, customId) {
        var stores = [];
        var lastPage = true;
        var page = (request.httpParameterMap && request.httpParameterMap.page.submitted) ? request.httpParameterMap.page : 1;

        if (this.apiKey) {
            var service = Svc.LocalServiceRegistry.createService('Leadformance.search', {
                createRequest: function(svc, args) {
                    var apiKey = args.apiKey;
                    svc.setRequestMethod('GET');
                    svc.addHeader('Authorization','Bearer ' + apiKey);
                    //for search by state name using world tree search cause search by query return no result
                    if (customId === 'world_tree_search' && !empty(args.query) && !empty(args.countryCode)) {
                        svc.setURL(svc.URL + '/search/world_tree.json');
                        svc.addParam('path', args.countryCode + '/' + args.query.toLowerCase().replace(' ', '-'));
                    } else {
                        //if we don't have zip code and country code, get all point of sales
                        if (empty(args.query) && empty(args.countryCode) && empty(args.customId)) {
                            svc.setURL(svc.URL + '/search/location.json');
                        } else {
                            svc.setURL(svc.URL + '/search.json');
                        }
                        if (!empty(args.query)) {
                            svc.addParam('query', args.query);
                        }
                        if (!empty(args.countryCode)) {
                            svc.addParam('country', args.countryCode);
                        }
                        if (!empty(args.customId)) {
                            svc.addParam('custom_id', args.customId);
                        }
                        svc.addParam('page', page);
                    }
                    if (args) {

                        return args;
                    } else {

                        return null;
                    }
                },
                parseResponse: function(svc, client) {

                    return JSON.parse(client.text);
                }
            });

            var callResult = service.call({
                'apiKey' : this.apiKey,
                'query' : query,
                'countryCode' : countryCode,
                'customId' : customId
            });

            if (callResult.status == 'OK') {
                if (!callResult.object.not_found && callResult.object.results) {
                    stores = this.parseResults(callResult.object.results);
                }
                if (callResult.object.total_entries) {
                    lastPage = (Math.ceil(callResult.object.total_entries / 20) <= page);
                }
            } else{
                Logger.error('[LeadformaceHelper.js] An error has been occured. Call Result : {0}', callResult);
            }

        } else {
            Logger.error('[LeadformaceHelper.js] Leadformance Api Key couldn\'t find on BM configurations.');
        }

        return {
            stores: stores,
            pagination: {isLastPage: lastPage, pageNumber: page}
        };
    };

    /**
     * Get stores from API services using latitude and longitude
     * @param {String} lat latitude
     * @param {String} lng longitude
     * @returns {Array} an array containing stores.
     */
    this.getStoresWithGeo = function(lat, lng) {
        var stores = [];
        var lastPage = true;
        var page = (request.httpParameterMap.page.submitted) ? request.httpParameterMap.page : 1;
        if (this.apiKey) {
            var service = Svc.LocalServiceRegistry.createService('Leadformance.geo', {
                createRequest: function(svc, args) {
                    var apiKey = args.apiKey;
                    svc.setURL(svc.URL + '/search/geo.json');
                    svc.setRequestMethod('GET');
                    svc.addHeader('Authorization','Bearer ' + apiKey);
                    svc.addParam('lat', args.lat);
                    svc.addParam('lng', args.lng);
                    svc.addParam('page', page);

                    if (args) {

                        return args;
                    } else {

                        return null;
                    }
                },
                parseResponse: function(svc, client) {

                    return JSON.parse(client.text);
                }
            });

            var callResult = service.call({
                'apiKey' : this.apiKey,
                'lat' : lat,
                'lng' : lng
            });

            if (callResult.status == 'OK') {
                stores = this.parseResults(callResult.object.results);
                lastPage = (Math.ceil(callResult.object.total_entries / 20) <= page);
            } else {
                Logger.error('[LeadformaceHelper.js] An error has been occured. Call Result : {0}', callResult);
            }

        } else {
            Logger.error('[LeadformaceHelper.js] Leadformance Api Key couldn\'t find on BM configurations.');
        }

        return {
            stores: stores,
            pagination: {isLastPage: lastPage, pageNumber: page}
        };
    };

    /**
     * Parsing store results which returns from API request
     * @param {Array} an array containing parsed stores.
     */
    this.parseResults = function(stores) {
        var availableStores = [];
        if (stores.length > 0) {
            for each(let store in stores) {
                if (!empty(store.custom_id)) {
                    store.ID = store.custom_id;
                     var _store = StoreMgr.getStore(store.ID);
                    if (_store) {
                        store.inventoryListId = _store.inventoryList ? _store.inventoryList.ID : '';
                        store.timezone = this.defineTimezone(!empty(store.city) ? store.city.toUpperCase() : '');
                        availableStores.push(store);
                    }
                }
            }
        }
        return availableStores;
    }

    /**
    * Returns timezone according to JSON map defined on Site Preferences
    * @param: city
    */
    this.defineTimezone = function(city) {
        var timezoneMap = !empty(Site.current.getPreferences()) ? Site.current.getCustomPreferenceValue('celTimezoneMap') : null;
        var timezone = 'Europe/Paris';

        if (timezoneMap && !empty(city)) {
            try {
                timezoneMap = JSON.parse(timezoneMap);
            } catch (e) {
                timezoneMap = null;
            }
            if (!empty(timezoneMap) && timezoneMap[city]) {
                timezone = timezoneMap[city];
            }
        }

        return timezone;
    }
}

module.exports = new LeadformanceHelper();
