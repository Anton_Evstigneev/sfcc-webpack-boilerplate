'use strict';

/**
 * Helper that provides Geolocation using Demandware MaxMind
/* API includes */
var Site = require('dw/system/Site');
var Cookie = require('dw/web/Cookie');
var Logger = require("dw/system/Logger");
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Countries = require('app_celine_core/cartridge/scripts/util/Countries');

/**
 * When the customer lands on the website, it checks his country code and redirects him to
 * corresponding locale. If there is no corresponding locale it will redirect him to the
 * default locale.
 */
function MaxMindIntegrationHelper() {

    /*
     * Sets a cookie by ajax call from with locale that user selects at footer 
     *
     * @param {String} newCountryCode - Locale value
     */
    this.updateCookieAjax = function(newCountryCode) {
        try {
            var returnObj = {}, 
                countryCode = newCountryCode.stringValue,
                locale; 
            if (this.isEnabled) { 
                locale = this.getLocale(countryCode.split('_')[1]);
                this.setCookie(locale.cookieValue);
                returnObj = {success : true};
                return returnObj;
            }
        }
        catch(e) {
            Logger.getLogger("MaxmindGeoLocation", "maxmindgeolocation").error("Unable to process maxmind ajax geolocation, error on line {0}, error: {1}", e.lineNumber, e.message);
            return returnObj;
        }
    };
	
    // Redirect the user to the appropriate locale using the Geolocation API
    this.redirect = function () { 
    	try {        
            var siteLocale = request.locale,
                geoLocation = request.getGeolocation(),
                countryCode = geoLocation.getCountryCode(),
                cookie = this.getCookie(),
                params = request.httpParameterMap,
                lang = params.isParameterSubmitted('lang') ? params.lang.stringValue : '',
                isRoot = request.httpHeaders['x-is-path_info'] == '/',
                redirectUrl, locale;  
                       
                if (isRoot && this.isEnabled ) {
                    // Set the cookie according to the countryCode from the Geolocation
                    if (cookie == false) {
                        locale = this.getLocale(countryCode);
                        redirectUrl = locale.redirectUrl;
                        this.setCookie(locale.cookieValue);
                        response.redirect(redirectUrl);
                        // Set the cookie using the locale (when the country selector is used)
                    } else { 
                    	locale = this.getLocale(cookie);
                    	redirectUrl = locale.redirectUrl;
                    	response.redirect(redirectUrl);
                    } 	
                    return;
                }
        } catch(e) {
        	var ex = e;
            Logger.getLogger("MaxmindGeoLocation", "maxmindgeolocation").error("Unable to process maxmind geolcation, error on line {0}, error: {1}", e.lineNumber, e.message);
            return;
        }
    };  

    /*
     * Determine the appropriate locale for the costomer
     *
     * @param {String} countryCode - Country code provided by the Geolocation / determined from locale
     * @return {Object} - Object containing the redirect url and the cookie value
     */
    this.getLocale = function (countryCode) {
        // Get the MaxMind Geolocation settings
        var mmCountry2SiteDefault = Site.current.getCustomPreferenceValue("maxMindCountryCodeToSiteDefault"),
            mmCountry2SiteMapping = JSON.parse(Site.current.getCustomPreferenceValue("maxMindCountryCodeToSiteMapping"))
    
        var siteID = Site.getCurrent().getCustomPreferenceValue('celNoneTrasactionalSiteID');
        var isTransactional = false;
        var countriesCode = Countries.getCountryByCode(countryCode);
        if (!empty(countriesCode)) {
        	isTransactional = countriesCode.eCommerce;	
        }
        if (isTransactional) {
            siteID = Site.getCurrent().getCustomPreferenceValue('celEcommerceSiteID');
        }      

        if (countryCode in mmCountry2SiteMapping) {
            redirectUrl = URLUtils.url(new dw.web.URLAction('Home-Show', siteID, mmCountry2SiteMapping[countryCode].split('=')[1]));
            cookieValue = countryCode;
        } else {
            redirectUrl = URLUtils.url(new dw.web.URLAction('Home-Show', siteID, mmCountry2SiteMapping[mmCountry2SiteDefault].split('=')[1]));
            cookieValue = mmCountry2SiteDefault;
        }
        return {
            redirectUrl: redirectUrl,
            cookieValue: cookieValue
        };
    };

    /*
     * Set a cookie with the users locale for future reference
     *
     * @param {String} locale - Cookie value
     */
    this.setCookie = function (locale) {
        // The cookie age is not set so the cookie will expire when the session ends
        var mm_glc = new Cookie("mm_glc", locale);
        mm_glc.setPath("/");
        mm_glc.setMaxAge(365*60*60*24);
        response.addHttpCookie(mm_glc);
    };

    // Check if the cookie is set
    this.getCookie = function () {
        var cookies = request.httpCookies;
        if (cookies.mm_glc) {
            return cookies.mm_glc.value;
        } else {
            return false;
        }
    };
    
    /**
     * Set a cookie with the users locale for future reference
     * @param {String} name - Cookie name
     * @param {String} value - Cookie value
     */
    this.setGeneralCookie = function (name, value) {
        var cookieS = new Cookie(name, value);
        cookieS.setPath("/");
        response.addHttpCookie(cookieS);
    };

    /**
     * Checks if the Cookie is set and return the value
     * @param {String} name - Cookie name
     * @return {boolean,string} cookie value
     */
    this.getGeneralCookie = function (name) {
        var cookies = request.getHttpCookies();
        if (cookies[name]) {
            return cookies[name].value;
        } else {
            return false;
        }
    };
      
    /**
     * Remove the cookie
     * @param {String} name - Cookie name
     */
    this.removeCookie = function (name) {
        var cookies = request.getHttpCookies()
        var cookie = cookies[name];
        cookie.setMaxAge(0);
        cookie.setPath('/');
        response.addHttpCookie(cookie);
    };
       
    // this function is used to check if cookies are enabled
    this.isCookieEnabled = function(){
        var isEnabled = true;
        if(request.getHttpCookies().getCookieCount() == 0) {
            //hack DW to determine cookie enabled/disabled on client's browser
            this.setGeneralCookie("celine_cookies_accepted", 1);
            if(!this.getGeneralCookie('celine_cookies_accepted')) {
                isEnabled = false; 
            }
            this.removeCookie("celine_cookies_accepted");

        }
        return isEnabled;
    };

    // Check if the MaxMind Geolocation is Enabled
    this.isEnabled = function () {
        return Site.current.getCustomPreferenceValue("maxMindIsEnabled");
    };
}

module.exports = MaxMindIntegrationHelper;