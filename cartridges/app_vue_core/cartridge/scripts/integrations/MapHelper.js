'use strict'
/**
 * Helper that providing Google Map integration informations
 */

/* API includes */
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');

/* Script Modules */
var sitepreferences = require('sitepreferences');

/**
 * @description Get google map api key
 * @return {String} api key
 */
function getGmapApiKey () {
    var apiKey = sitepreferences.get('celGmapApiKey');

    if (!empty(apiKey)) {

        return apiKey;
    } else {
        Logger.error('Google Map api key missed. You need set from Find In Store Configurations section on  BM.');
    }

}

/**
 * @description Get baidu map api key
 * @param {siteCountry} current site country
 * @returns {String} api key
 */
function getBaiduApiKey(siteCountry) {

    var apiKey = sitepreferences.get('celBaiduMapApiKey');

    if (siteCountry && siteCountry.countryCode == 'CN') {
        if (!empty(apiKey)) {

            return apiKey;
        } else {
            Logger.error('Baidu Map api key missed. You need set from Find In Store Configurations section on  BM.');
        }
    } 

}

exports.GetGmapApiKey = getGmapApiKey;
exports.GetBaiduApiKey = getBaiduApiKey;

