'use strict';

var Resource = require('dw/web/Resource');

/**
 * @module meta
 */

var HOME_BREADCRUMB = {
    name: dw.web.Resource.msg('global.home', 'locale', null),
    url: dw.web.URLUtils.httpHome()
};

/**
 * Constructor for metadata singleton
 *
 * This should be initialized via the current context object (product, category, asset or folder) and can
 * be used to retrieve the page metadata, breadcrumbs and to render the accumulated information to the client
 *
 * @class
 */
var Meta = function() {
    this.data = {
        page: {
            title: '',
            description: '',
            keywords: ''
        },
        // supports elements with properties name and url
        breadcrumbs: [HOME_BREADCRUMB],
        resources: {}
    };
};

Meta.prototype = {
    /**
     * The core method of this class which updates the internal data represenation with the given information
     *
     * @param  {Object|dw.catalog.Product|dw.catalog.Category|dw.content.Content|dw.content.Folder} object The object to update with
     *
     * @example
     * // using a product object
     * meta.update(product);
     * // using a plain object
     * meta.update({resources: {
     *     'MY_RESOURCE': dw.web.Resource.msg('my.resource', 'mybundle', null)
     * }});
     * // using a string
     * meta.update('account.landing')
     */
    update: function(object) {
        // if object is null, then don't try to update it
        if (object === null) {
            return;
        }

        // check if object wrapped in AbstractModel and get system object if so get the system object
        if ('object' in object) {
            object = object.object;
        }
        // check if it is a system object
        if (object.class) {
            // update metadata
            var title = null;
            if ('pageTitle' in object) {
                title = object.pageTitle;
            }
            if (!title && 'name' in object) {
                title = object.name;
            } else if (!title && 'displayName' in object) {
                title = object.displayName;
            }
            this.data.page.title = title;
            if ('pageKeywords' in object && object.pageKeywords) {
                this.data.page.keywords = object.pageKeywords;
            }
            if ('pageDescription' in object && object.pageDescription) {
                this.data.page.description = object.pageDescription;
            }

            this.updatePageMetaDataWithSeo(object);

            // Update breadcrumbs for content
            if (object.class === dw.content.Content) {
                var path = require('~/cartridge/scripts/models/ContentModel').get(object).getFolderPath();
                this.data.breadcrumbs = path.map(function(folder) {
                    return {
                        name: folder.displayName
                    };
                });
                this.data.breadcrumbs.unshift(HOME_BREADCRUMB);
                this.data.breadcrumbs.push({
                    name: object.name,
                    url: dw.web.URLUtils.url('Page-Show', 'cid', object.ID)
                });
                dw.system.Logger.debug('Content breadcrumbs calculated: ' + JSON.stringify(this.data.breadcrumbs));
            }
        } else if (typeof object === 'string') {
            // @TODO Should ideally allow to pass something like account.overview, account.wishlist etc.
            // and at least generate the breadcrumbs & page title
        } else {
            if (object.pageTitle) {
                this.data.page.title = object.pageTitle.trim();
            }
            if (object.pageKeywords) {
                this.data.page.keywords = object.pageKeywords.trim();
            }
            if (object.pageDescription) {
                this.data.page.description = object.pageDescription.trim();
            }
            // @TODO do an _.extend(this.data, object) of the passed object
        }
        this.updatePageMetaData();
    },
    /**
     * Update the Page Metadata with the current internal data
     */
    updatePageMetaData: function() {
        var pageMetaData = request.pageMetaData;
        pageMetaData.title = this.data.page.title;
        pageMetaData.keywords = this.data.page.keywords;
        pageMetaData.description = this.data.page.description;
    },
    /**
     * Get the breadcrumbs for the current page
     *
     * @return {Array} an array containing the breadcrumb items
     */
    getBreadcrumbs: function() {
        return this.data.breadcrumbs || [];
    },
    /**
     * Adds a resource key to meta, the key of the given bundle is simply dumped to a data
     * attribute and can be consumed by the client.
     *
     * @example
     * // on the server
     * require('meta').addResource('some.message.key', 'bundlename');
     * // on the client
     * console.log(app.resources['some.message.key']);
     *
     * @param {string} key          The resource key
     * @param {string} bundle       The bundle name
     * @param {string} defaultValue Optional default value, empty string otherwise
     */
    addResource: function(key, bundle, defaultValue) {
        this.data.resources[key] = dw.web.Resource.msg(key, bundle, defaultValue || '');
    },
    /**
     * Dumps the internally held data into teh DOM
     *
     * @return {String} A div with a data attribute containing all data as JSON
     */
    renderClientData: function() {
        return '<div class="page-context" data-dw-context="' + JSON.stringify(this.data) + '" />';
    },

    updatePageMetaDataWithSeo: function(object) {
        var currentCountry = require('app_celine_core/cartridge/scripts/util/Countries').getCurrent({
            CurrentRequest: request
        });

        var overridenTitle = null;
        var overridenDesc = null;

        // Override values
        if (('masterProduct' in object) || ('constructor' in object && object.constructor != null && object.constructor.name == 'dw.catalog.Category')) {
            if (!empty(object.pageTitle)) {
                this.data.page.title = overridenTitle = object.pageTitle.trim();
            }
            if (!empty(object.pageDescription)) {
                this.data.page.description = overridenDesc = object.pageDescription.trim();
            }
        }

        if ('masterProduct' in object) {
            var productName = this.data.page.title.trim();
            var product = object;

            if (product.onlineCategories.length < 1) {
                return;
            }
            var primaryCat = product.primaryCategory.online ? product.primaryCategory : product.onlineCategories[0];
            var currCategoryLevel = 1;
            while (!!primaryCat && primaryCat.parent != null) {
                primaryCat = primaryCat.parent;
                currCategoryLevel += 1;
            }
            var catLevel2, catLevel3;
            if (currCategoryLevel == 2) {
                catLevel2 = product.primaryCategory.displayName.trim();
                catLevel3 = product.primaryCategory.displayName.trim();
            } else if (currCategoryLevel == 3) {
                catLevel2 = product.primaryCategory.parent.displayName.trim();
                catLevel3 = product.primaryCategory.displayName.trim();
            } else {
                return;
            }


            var newMetaTitle = productName + " - " + catLevel2 + " | " + Resource.msg('seo.catalog.product.title', 'seo', null);
            if (newMetaTitle.length > 69) {
                newMetaTitle = productName + " | " + Resource.msg('seo.catalog.product.title', 'seo', null);
            }
            if (newMetaTitle.length > 69) {
                newMetaTitle = productName;
            }

            // For Ecommerce website
            if (currentCountry.eCommerce) {
                var newMetaDesc = Resource.msgf('seo.catalog.product.ecom.meta', 'seo', null, productName, catLevel3.toLowerCase());
                if (newMetaDesc.length > 156) {
                    newMetaDesc = Resource.msgf('seo.catalog.product.ecom.meta', 'seo', null, productName.substr(0, productName.length - (newMetaDesc.length - 156)), catLevel3.toLowerCase());
                }
                // For NonEcommerce Website
            } else {
                var newMetaDesc = Resource.msgf('seo.catalog.product.nonecom.meta', 'seo', null, productName, catLevel3.toLowerCase());
                if (newMetaDesc.length > 156) {
                    newMetaDesc = Resource.msgf('seo.catalog.product.nonecom.meta', 'seo', null, productName.substr(0, productName.length - (newMetaDesc.length - 156)), catLevel3.toLowerCase());
                }
            }

        } else if ('constructor' in object && object.constructor != null && object.constructor.name == 'dw.catalog.Category') {
            var currCategory = object;
            var currCategoryLevel = 1;
            while (!!currCategory && currCategory.parent != null) {
                currCategory = currCategory.parent;
                currCategoryLevel += 1;
            }

            var catLevel2;
            var catLevel2Id;
            if (currCategoryLevel == 1) {
                return;
            } else {
                if (currCategoryLevel == 2) {
                    catLevel2 = object.displayName.trim();
                    catLevel2Id = object.ID.toLowerCase();
                } else if (currCategoryLevel == 3) {
                    var catLevel3 = object.displayName.trim();
                    var catLevel3Id = object.ID.toLowerCase();
                    catLevel2 = object.parent.displayName.trim();
                    catLevel2Id = object.parent.ID.toLowerCase();
                } else {
                    return;
                }

                if (typeof catLevel3 != 'undefined') {
                    var newMetaTitle = catLevel3 + " - " + catLevel2 + " | " + Resource.msg('seo.catalog.category.title', 'seo', null);
                } else {
                    var newMetaTitle = catLevel2 + " | " + Resource.msg('seo.catalog.category.title', 'seo', null);
                }

                // For Ecommerce website
                if (currentCountry.eCommerce) {
                    var newMetaDesc = Resource.msgf('seo.catalog.toplevel.category.meta.ecom.' + catLevel2Id, 'seo', null);
                    // For NonEcommerce Website
                } else {
                    var newMetaDesc = Resource.msgf('seo.catalog.toplevel.category.meta.nonecom.' + catLevel2Id, 'seo', null);
                }
            }
        } else {
            return;
        }
        if (typeof newMetaTitle == 'undefined') {
            return;
        }

        this.data.page.title = overridenTitle || newMetaTitle.trim();
        this.data.page.description = overridenDesc || newMetaDesc.trim();
    }
};

/**
 * Singleton instance for meta data handling
 * @type {module:meta~Meta}
 */
module.exports = new Meta();