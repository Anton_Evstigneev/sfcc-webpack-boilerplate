'use strict';

/**
 * Module for ordering functionality.
 * @module models/OrderModel
 */

/* API Includes */
var AbstractModel = require('./AbstractModel');
var Order = require('dw/order/Order');
var OrderMgr = require('dw/order/OrderMgr');
var Resource = require('dw/web/Resource');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var ContentMgr = require('dw/content/ContentMgr');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('app_celine_controllers/cartridge/scripts/app');
var StocksHelper = require('app_celine_core/cartridge/scripts/util/StocksHelper');
var LineItemCtnrHelpers = require('app_celine_core/cartridge/scripts/order/LineItemCtnrHelpers.js');
var ProductUtils = require('app_celine_core/cartridge/scripts/product/ProductUtils.js');
var STOCK_CONSTANTS = StocksHelper.getConstants();

/* Models */
var Content = app.getModel('Content');

/**
 * Place an order using OrderMgr. If order is placed successfully,
 * its status will be set as confirmed, and export status set to ready.
 * @param {dw.order.Order} order
 */
function placeOrder(order) {
    var placeOrderStatus = OrderMgr.placeOrder(order);
    if (placeOrderStatus === Status.ERROR) {
        OrderMgr.failOrder(order);
        throw new Error('Failed to place order.');
    }
    order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
    order.setExportStatus(Order.EXPORT_STATUS_READY);
}
/**
 * Order helper class providing enhanced order functionality.
 * @class module:models/OrderModel~OrderModel
 * @extends module:models/AbstractModel
 *
 * @param {dw.order.Order} obj The order object to enhance/wrap.
 */
var OrderModel = AbstractModel.extend({

});

/**
 * Gets a new instance for a given order or order number.
 *
 * @alias module:models/OrderModel~OrderModel/get
 * @param parameter {dw.order.Order | String} The order object to enhance/wrap or the order ID of the order object.
 * @returns {module:models/OrderModel~OrderModel}
 */
OrderModel.get = function(parameter) {
    var obj = null;
    if (typeof parameter === 'string') {
        obj = OrderMgr.getOrder(parameter);
    } else if (typeof parameter === 'object') {
        obj = parameter;
    }
    return new OrderModel(obj);
};

/**
 * Submits an order
 * @param order {dw.order.Order} The order object to be submitted.
 * @transactional
 * @return {Object} object If order cannot be placed, object.error is set to true. Ortherwise, object.order_created is true, and object.Order is set to the order.
 */
OrderModel.submit = function(order) {
    var GiftCertificate = require('./GiftCertificateModel');

    try {
        Transaction.begin();
        placeOrder(order);

        // Creates gift certificates for all gift certificate line items in the order
        // and sends an email to the gift certificate receiver

        order.getGiftCertificateLineItems().toArray().map(function(lineItem) {
            return GiftCertificate.createGiftCertificateFromLineItem(lineItem, order.getOrderNo());
        }).forEach(GiftCertificate.sendGiftCertificateEmail);

        Transaction.commit();
    } catch (e) {
        Transaction.rollback();
        return {
            error: true,
            PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical')
        };
    }

    return {
        Order: order,
        order_created: true
    };
}

/**
 * Returns prices related with order
 * @param order {dw.order.Order} The order object
 * @return {Object} object with prices values
 */
OrderModel.getPrices = function(order) {
    var taxPrice = order.getTotalTax().getValue();
    var shippingPrice = order.getShippingTotalPrice().getValue();
    var totalPrice = (order.getTotalGrossPrice().getValue()).toFixed(2);
    var currencyCode = order.getCurrencyCode();

    var merchTotalExclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(false);
    var merchTotalInclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(true);
    var orderDiscount = merchTotalExclOrderDiscounts.subtract(merchTotalInclOrderDiscounts);
    var totalStandartPrice = dw.value.Money(0, order.getCurrencyCode()),
        totalSalesPrice = dw.value.Money(0, order.getCurrencyCode()),
        products = order.getProductLineItems().iterator().asList();
    for (var i = 0; i < products.length; i++) {
        totalStandartPrice = totalStandartPrice.add(products[i].getBasePrice().multiply(products[i].getQuantity()));
        totalSalesPrice = totalSalesPrice.add(products[i].getAdjustedGrossPrice());
    }
    orderDiscount = orderDiscount.add(totalStandartPrice.subtract(totalSalesPrice));

    if (shippingPrice == 0) {

        var shipment = order.shipments.iterator().next();
        var shippingName = shipment && shipment.shippingMethod ? shipment.shippingMethod.displayName : '';
        if (shipment && shipment.shippingMethod && shipment.shippingMethod.custom.storePickupEnabled) {
            var shippingTotal = Resource.msg('summary.collectinstore', 'checkout', null);
        } else {
            var shippingTotal = Resource.msgf('order.orderconfirmation-email.shippingfree', 'order', null, shippingName);
        }

    } else {
        var shippingTotal = Resource.msgf('resource.pricewithcurrency', 'email', null, shippingPrice, currencyCode);
        
        if (order.getDefaultShipment().shippingMethodID == STOCK_CONSTANTS.METHOD_ID_CONCIERGE) {
            shippingTotal = Resource.msgf('order.orderconfirmation-email.shippingconcierge', 'order', null, shippingTotal);
        }
    }
    
    var salesTotalDiscount = ProductUtils.formatPrice(LineItemCtnrHelpers.getDiscountedValue(order));

    var prices = {
        taxTotal: Resource.msgf('resource.pricewithcurrency', 'email', null, taxPrice, currencyCode),
        shippingTotal: shippingTotal,
        total: Resource.msgf('resource.pricewithcurrency', 'email', null, totalPrice, currencyCode),
        orderDiscount: orderDiscount || null,
        salesTotalDiscount: salesTotalDiscount
    }
    return prices;
}

/** The order class */
module.exports = OrderModel;