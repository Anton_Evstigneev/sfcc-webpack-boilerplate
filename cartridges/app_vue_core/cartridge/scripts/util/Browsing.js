'use strict';

/**
 * @module util/Browsing
 */

var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');

/**
 * Recovers the last url from the click stream
 * @return {dw.web.URL} the last called URL
 */
exports.lastUrl  = function lastUrl() {
    var location = URLUtils.url('Home-Show');
    var click = session.clickStream.last;
    if (click) {
        location = URLUtils.url(click.pipelineName);
        if (!empty(click.queryString) && click.queryString.indexOf('=') !== -1) {
            var params = click.queryString.split('&');
            params.forEach(function (param) {
                location.append.apply(location,param.split('='));
            });
        }
    }
    return location;
};

/**
 * Returns the last catalog URL or homepage URL if non found
 * @return {String} The last browsed catalog URL
 */
exports.lastCatalogURL = function lastCatalogURL() {
    var clicks = session.getClickStream().getClicks();

    for (var i = clicks.size() - 1; i >= 0; i--) {
        var click = clicks[i];
        switch (click.getPipelineName()) {
            case 'Product-Show':
            case 'Search-Show':
                // catalog related click
                // replace well-known http parameter names 'source' and 'format' to avoid loading partial page markup only
                return 'http://' + click.host + click.url.replace(/source=/g, 'src=').replace(/format=/g, 'frmt=');
        }
    }

    return URLUtils.httpHome().toString();
};

/**
 * Returns the last product URL or homepage URL if non found
 * @return {String} The last browsed product URL
 */
exports.lastProductURL = function lastProductURL() {
    var clicks = session.getClickStream().getClicks().toArray().reverse();
    var lastProductURL = URLUtils.httpHome();

    for (let i = 0; i < clicks.length; i++) {
        if (clicks[i].pipelineName == 'Product-Show') {
            let pid = clicks[i].queryString.substr(4);
            lastProductURL = URLUtils.https('Product-Show', 'pid', pid);
            break;
        }
    }

    return lastProductURL.toString();
}

/**
 * Returns the last URL user browsed from the main pages
 * @return {String} The last browsed URL from the specified controllers
 */
exports.previousPageURL = function previousPageURL() {
    var listOfControllers;
    var clicks = session.getClickStream().getClicks();
    var previousPageURL = URLUtils.httpHome();
    try {
        // get list of controllers from site preference
        listOfControllers = JSON.parse(Site.getCurrent().getCustomPreferenceValue('celListOfControllers'));
        if (listOfControllers) {
            listOfControllers = listOfControllers.pages;
        }
    } catch (e) {
        Logger.warn('JSON format is incorrect for list of controllers');
        return previousPageURL.toString();
    }

    for (let i = clicks.size() - 1; i >= 0; i--) {
        var click = clicks[i];
        // if previous page is from the list of controllers return to this page, in other case to home page
        if (listOfControllers.indexOf(click.getPipelineName()) != -1) {
            if (click.pipelineName == 'Login-Show') {
                return click.referer + '?open=loginpopup';
            }
            return 'https://' + click.host + click.url.replace(/source=/g, 'src=').replace(/format=/g, 'frmt=');
        }
    }

    return previousPageURL;

}
//add url parser (libUrl)
//add continue shopping here
