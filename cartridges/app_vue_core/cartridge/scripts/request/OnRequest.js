'use strict';

/**
 * The onRequest hook is called with every top-level request in a site. This happens both for requests to cached and non-cached pages.
 * For performance reasons the hook function should be kept short.
 *
 * @module  request/OnRequest
 */

var Status = require('dw/system/Status');

/**
 * The onRequest hook function.
 */
exports.onRequest = function () {

    var CookieUtils = require('app_celine_core/cartridge/scripts/util/CookieUtils');
    CookieUtils.handleCurrency();
    if (!request.isIncludeRequest() && request.httpMethod == 'GET') {
        if(!empty(dw.system.Site.current) && dw.system.Site.current.getCustomPreferenceValue('maxMindIsEnabled')) {
            new (require('app_celine_controllers/cartridge/scripts/integrations/MaxMindIntegrationHelper'))().redirect();
        }
    }

    return new Status(Status.OK);
};
