import routes from '@app_vue_core_js/router/routes';

describe('Sample test', () => {
    step('Routes is instance of Object', function () {
        assert(routes instanceof Object, 'routes is not instance of Object');
    });

    step('Promise should resolve to a proper value', mochaAsync( async function () {
        // example on how to increase default timeout (2000ms) before test case fails
        this.timeout(2500);

        const samplePromise = new Promise(function (resolve) {
            setTimeout(() => {
                resolve('Sample result');
            }, 2000);
        });

        assert(await samplePromise === 'Sample result', 'Promise haven\'t resolved to correct value');
    }));
});