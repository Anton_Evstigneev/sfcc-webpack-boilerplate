import Vue from 'vue';
import Vuex from 'vuex';

import homeModule from './modules/home';
import PDPModule from './modules/pdp';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        currentLocale: 'en_US'
    },
    mutations: {
        setLocale(state, locale) {
            state.currentLocale = locale;
        }
    },
    modules: {
        home: homeModule,
        pdp: PDPModule
    }
});

export default store;