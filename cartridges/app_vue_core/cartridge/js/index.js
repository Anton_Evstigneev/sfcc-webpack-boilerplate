// Babel polyfill
import '@babel/polyfill';

// libs
import Vue from 'vue';

// components
import store from './store';
import router from './router';
import App from '@app_vue_core_js/globalVueComponents/app.vue';

document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        new Vue({
            el: '#vue-app',
            store: store,
            router: router,
            render(h) {
                return h(App, {});
            }
        });
    }
};
