import HomePage from 'js/pages/home/home.vue';
import PDPPage from 'js/pages/pdp/pdp.vue';

const routes = [
    { path: '/', name: 'home', component: HomePage },
    { path: '/product/:productId', name: 'pdp', component: PDPPage }
];

export default routes;