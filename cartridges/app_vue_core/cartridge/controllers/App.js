'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Home
 */

// API imports
var ISML = require('dw/template/ISML');

// modules
var guard = require('~/cartridge/scripts/guard');

function show() {
    ISML.renderTemplate('application');
}

/*
 * Export the publicly available controller methods
 */
/** Renders the home page.
 * @see module:controllers/Home~show */
exports.Show = guard.ensure(['get'], show);